import { Component, OnInit } from '@angular/core';
import { HeroeModel } from '../../models/heroe.model';
import { NgForm } from '@angular/forms';
import { HeroesService } from '../../services/heroes.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';



import swal from 'sweetalert2';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe = new HeroeModel(); // estoy asignando el modelo

  constructor(private heroeservice: HeroesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id'); // me trae un heroe
    if (id !== 'nuevo'){
      this.heroeservice.getHeroe(id)
      .subscribe((resp: HeroeModel) => {
        this.heroe = resp;
        this.heroe.id = id;
        console.log(resp);
      });
    }

  }

  guardar(form: NgForm) {
    if (form.invalid) {
      console.log('Formulario no válido');
      return;
    }
    // trabajamos con el sweetAlert
    swal.fire({
      title: 'Espere',
      text: 'Guardando Informacion',
      icon: 'info',
      allowOutsideClick: false
    });
    swal.showLoading();

    let peticion: Observable<any>;

    if (this.heroe.id) {
      peticion = this.heroeservice.actualizarHeroe(this.heroe);

    } else {
      peticion = this.heroeservice.crearHeroe(this.heroe);
      }
    peticion.subscribe(resp => {
     swal.fire({
      title: this.heroe.nombre,
      text: 'Se actualizó correctamente',
      icon: 'success'

     });
     console.log(resp);
    });
  }


}
