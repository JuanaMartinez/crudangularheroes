import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeroeModel } from '../models/heroe.model';
import { map, delay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private url = 'https://heroes-9e742.firebaseio.com';

  constructor(private http: HttpClient) { }

  crearHeroe(heroe: HeroeModel){

    return this.http.post(`${this.url}/heroes.json`, heroe) // el método post necestia la url(endpoint), más el body
             // método pipe de los observables
             .pipe(
               map((resp: any) => {
                 heroe.id = resp.name;
                 return heroe;
               })
             );

  }
  actualizarHeroe(heroe: HeroeModel){
    const heroeTemp = {
      ...heroe

    };
    delete heroeTemp.id;
    return this.http.put(`${this.url}/heroes/${heroe.id}.json`, heroeTemp);
  }
  // obtener heroe por id
  getHeroe(id: string){
    return this.http.get(`${this.url}/heroes/${id}.json`);

  }
  borrarHeroe(id: string){
    return this.http.delete(`${this.url}/heroes/${id}.json`);

  }
  // traigo todos los heroes
  getHeroes(){
    return this.http.get(`${this.url}/heroes.json`)
          .pipe (
            map (this.crearArreglo ), // el map transforma la información en este caso en un arreglos
            //  map (resp => this.crearArreglo(resp) ) esto es exactamente igual al de arriba
            delay(500) // retrasa la carga de datos en milisegundos
          );
  }
  private crearArreglo(heroesObj: object ){
    const heroes: HeroeModel[] = [];

   // console.log(heroesObj);

    if (heroesObj == null) {return []; } // validación si el heoreobj es null retorna un objeto vacio
    Object.keys(heroesObj).forEach(key => {
      const heroe: HeroeModel = heroesObj[key];
      heroe.id = key;
      heroes.push(heroe);

    });
    return heroes;
  }
}
