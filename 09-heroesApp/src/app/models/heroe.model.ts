export class HeroeModel {
    id: string;
    nombre: string;
    poder: string;
    vivo: boolean;
// en el constructor inicializamos a vivo(true)
    constructor(){
        this.vivo = true;
    }
}
